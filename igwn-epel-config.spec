Name:      igwn-epel-config
Version:   8
Release:   2%{?dist}
Summary:   DNF configuration for IGWN EPEL Snapshot Repository.

License:   GPLv3+
BuildArch: noarch
Requires:  redhat-release >= %{version}
URL:       https://software.igwn.org/lscsoft/epel/%{version}

Source0:   igwn-epel.repo
Source1:   COPYING
Source2:   README-igwn-epel.md

%description
DNF configuration for IGWN EPEL Snapshot Repository on Rocky Linux %{version}

%prep
%setup -q -c -T
install -pm 644 %{SOURCE1} .
install -pm 644 %{SOURCE2} .

%build

%install
install -dm 755 %{buildroot}%{_sysconfdir}/yum.repos.d
install -pm 644 %{SOURCE0} %{buildroot}%{_sysconfdir}/yum.repos.d

%clean
rm -rf $RPM_BUILD_ROOT

%files
%doc README-igwn-epel.md
%license COPYING
%config(noreplace) %{_sysconfdir}/yum.repos.d/igwn-epel.repo

# dates should be formatred using: 'date +"%a %b %d %Y"'
%changelog
* Tue Apr 18 2023 Adam Mercer <adam.mercer@ligo.org> 8-2
- Fix repository description

* Tue Apr 18 2023 Adam Mercer <adam.mercer@ligo.org> 8-1
- initial version
