# IGWN EPEL Yum/DNF Repository

The IGWN EPEL Yum/DNF Repository configuration for the IGWN EPEL snapshot.

See <https://computing.docs.ligo.org/guide/software/> for details on
IGWN Software practices.
